use std::cell::RefCell;
use std::collections::HashSet;
use std::fmt::{Debug, Display, Formatter};

/// Types du langage
#[derive(Clone)]
pub enum Type {
    /// Variable
    Var(usize),
    /// Fonction (a -> b)
    Arrow(Box<Type>, Box<Type>),
    /// Entiers
    N,
    /// Liste (type)
    List(Box<Type>),
    /// Généralisation (var, type)
    Forall(usize, Box<Type>),
    /// Booléens
    Bool,
    /// Unit
    Unit,
    /// Région (type)
    Ref(Box<Type>),
}

// De même que pour Term, on implémente Display pour Type
impl Display for Type {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Type::Var(n) => write!(f, "Var({})", n),
            Type::Arrow(t1, t2) => write!(f, "({} -> {})", t1, t2),
            Type::N => write!(f, "Int"),
            Type::List(t) => write!(f, "List({})", t),
            Type::Forall(v, t) => write!(f, "Forall({}, {})", v, t),
            Type::Bool => write!(f, "Bool"),
            Type::Unit => write!(f, "()"),
            Type::Ref(t) => write!(f, "Ref({})", t),
        }
    }
}

// Idem pour Debug
impl Debug for Type {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self)
    }
}

// Implémentation de l'égalité pour les types
impl PartialEq<Self> for Type {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Type::Var(n1), Type::Var(n2)) => n1 == n2,
            (Type::Arrow(t1, t2), Type::Arrow(t3, t4)) => (t1 == t3) && (t2 == t4),
            (Type::N, Type::N) => true,
            (Type::List(t1), Type::List(t2)) | (Type::Ref(t1), Type::Ref(t2)) => t1 == t2,
            (Type::Forall(v1, t1), Type::Forall(v2, t2)) => {
                let newv = Type::fresh_var();
                t1.substitue(*v1, newv.clone()) == t2.substitue(*v2, newv)
            }
            (Type::Bool, Type::Bool) => true,
            (Type::Unit, Type::Unit) => true,
            (_, _) => false,
        }
    }
}

// Variable statique locale au thread
thread_local!(static TYPE_VAR_COUNTER: RefCell<usize> = RefCell::new(0));
impl Type {
    /// Création d'une nouvelle variable de type unique
    pub fn fresh_var() -> Type {
        TYPE_VAR_COUNTER.with(|n| *n.borrow_mut() += 1);
        Type::Var(TYPE_VAR_COUNTER.with(|n| *n.borrow()))
    }

    /// Création du type d'une fonction
    /// # Arguments
    /// * `t1` - Le type de l'argument de la fonction
    /// * `t2` - Le type de retour de la fonction
    /// # Retour
    /// Une type flèche correspondant au type de fonction souhaité
    pub fn mk_arrow(t1: Type, t2: Type) -> Type {
        Type::Arrow(Box::new(t1), Box::new(t2))
    }

    /// Fonction de vérification d'occurence d'une variable de type dans un type donné
    /// # Arguments
    /// * `v` - La variable, identifiée par son numéro
    /// # Retour
    /// `true` si `v` se trouve dans le type, `false` sinon
    pub fn occur_check(&self, v: usize) -> bool {
        match self {
            Type::Var(n) => v == *n,
            Type::Arrow(t1, t2) => t1.occur_check(v) || t2.occur_check(v),
            Type::N | Type::Bool | Type::Unit => false,
            Type::List(t) | Type::Forall(_, t) | Type::Ref(t) => t.occur_check(v),
        }
    }

    /// Substitue une variable par un type donné au sein d'un autre type
    /// # Arguments
    /// * `v` - Le nombre associé à la variable à substituer
    /// * `ts` - Le type par lequel on substitue
    /// # Retour
    /// Un type calqué sur celui de départ dans lequel la substitution a été effectuée
    pub fn substitue(&self, v: usize, ts: Type) -> Type {
        match self {
            Type::Var(n) => {
                if *n == v {
                    ts
                } else {
                    self.clone()
                }
            }
            Type::Arrow(t1, t2) => {
                let t3 = t1.substitue(v, ts.clone());
                let t4 = t2.substitue(v, ts);
                Type::mk_arrow(t3, t4)
            }
            Type::N | Type::Bool | Type::Unit => self.clone(),
            Type::List(t) => Type::List(Box::new(t.substitue(v, ts))),
            Type::Forall(n, t) => Type::Forall(*n, Box::new(t.substitue(v, ts))),
            Type::Ref(t) => Type::Ref(Box::new(t.substitue(v, ts))),
        }
    }

    /// Renvoie l'ensemble des variables de type libres dans un type donné
    /// # Arguments
    /// * `env` - l'environnement de type listant les variables liées
    /// * `set` - l'ensemble des variables libres en cours de construction
    /// # Retour
    /// L'ensemble des variables de type libres dans un type donné
    fn free_vars(&self, env: &[usize], mut set: HashSet<usize>) -> HashSet<usize> {
        match self {
            Type::Var(n) => {
                if !env.contains(n) {
                    set.insert(*n);
                }
                set
            }
            Type::Arrow(t1, t2) => {
                let tmp_list = t1.free_vars(env, set);
                t2.free_vars(env, tmp_list)
            }
            Type::N | Type::Bool | Type::Unit => set,
            Type::List(t) | Type::Ref(t) => t.free_vars(env, set),
            Type::Forall(var, bod) => {
                let mut newenv = env.to_owned();
                newenv.push(*var);
                bod.free_vars(&newenv, set)
            }
        }
    }

    /// Généralise un type sur toutes ses variables libres
    pub fn generalise(&self) -> Type {
        let free_vars = self.free_vars(&Vec::new(), HashSet::new());
        let mut res = self.clone();
        for var in free_vars {
            res = Type::Forall(var, Box::new(res))
        }
        res
    }
}

/// Une équation de deux types
pub type Equation = (Type, Type);
