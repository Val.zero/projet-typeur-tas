mod terms;
mod types;

#[macro_use]
extern crate lalrpop_util;

lalrpop_mod!(parser);

use crate::terms::*;
use std::fs;
use structopt::StructOpt;

/// Configuration options
#[derive(StructOpt, Debug)]
#[structopt(name = "config")]
struct Opt {
    /// File path to parse a specific program
    #[structopt(short = "f", long, default_value = "")]
    file: String,

    /// Number of the sample to use
    #[structopt(short = "s", long, default_value = "1")]
    sample: usize,
}

fn main() {
    let opt = Opt::from_args();
    let path = if opt.file == "" {
        format!("./samples/sample{:03}", opt.sample)
    } else {
        opt.file
    };
    let prog_string = fs::read_to_string(path).unwrap();
    println!("Read program:\n{}\n", prog_string);

    let prog: Term = *parser::TermParser::new().parse(&prog_string).unwrap();
    println!("Parsed program:\n{}", prog);
    println!("Prog type: {}\n", prog.typeur());

    let reduced_prog = prog.evaluateur(100);
    println!(
        "Can be reduced to {} of type {}",
        reduced_prog,
        reduced_prog.typeur()
    );
}

// TODO: put tests in a separate file
#[cfg(test)]
mod tests {
    use crate::terms::*;
    use crate::types::*;
    use std::io::Write;
    use std::{fs, io};

    lalrpop_mod!(parser);

    #[test]
    fn fresh_var_test() {
        let x1 = Term::fresh_var();
        let x2 = Term::fresh_var();
        let x3 = Term::Var("a".to_string());
        assert_ne!(x1, x2);
        assert_ne!(x1, x3);
        assert_ne!(x2, x3)
    }

    #[test]
    fn barendregt_test() {
        let id = Term::Abs("x".to_string(), Box::new(Term::Var("x".to_string())));
        let id_app = Term::App(Box::new(id.clone()), Box::new(id.clone()));
        let id_app2 = Term::App(Box::new(id), Box::new(id_app));
        let id_app3 = id_app2.barendregt(&mut Mem::new());
        let test = Term::App(
            Box::new(Term::Abs(
                "x1".to_string(),
                Box::new(Term::Var("x1".to_string())),
            )),
            Box::new(Term::App(
                Box::new(Term::Abs(
                    "x2".to_string(),
                    Box::new(Term::Var("x2".to_string())),
                )),
                Box::new(Term::Abs(
                    "x3".to_string(),
                    Box::new(Term::Var("x3".to_string())),
                )),
            )),
        );
        assert_eq!(id_app3, test)
    }

    #[test]
    fn instantie_test() {
        let v = Term::Var("a".to_string());
        let f = Term::Abs("x".to_string(), Box::new(Term::Var("x".to_string())));
        let app = Term::App(Box::new(f.clone()), Box::new(v));
        let sub = app.instantie(&mut Mem::new(), "a", Term::Var("b".to_string()));
        let res = Term::App(Box::new(f), Box::new(Term::Var("b".to_string())));
        let nosub = app.instantie(&mut Mem::new(), "z", Term::Var("c".to_string()));
        assert_eq!(sub, res);
        assert_eq!(app, nosub)
    }

    #[test]
    fn eveluateur_test() {
        let v = Term::Var("a".to_string());
        let f = Term::Abs(
            "x".to_string(),
            Box::new(Term::Binop(
                BinOp::Add,
                Box::new(Term::Var("x".to_string())),
                Box::new(Term::Var("x".to_string())),
            )),
        );
        let app = Term::App(Box::new(f), Box::new(v));
        let int = Term::Int(42);
        let let_ = Term::Let("a".to_string(), Box::new(int), Box::new(app));
        assert_eq!(let_.evaluateur(100), Term::Int(84))
    }

    #[test]
    fn typeur_test() {
        let v = Term::Var("a".to_string());
        let f = Term::Abs(
            "x".to_string(),
            Box::new(Term::Binop(
                BinOp::Add,
                Box::new(Term::Var("x".to_string())),
                Box::new(Term::Var("x".to_string())),
            )),
        );
        let app = Term::App(Box::new(f), Box::new(v));
        let int = Term::Int(42);
        let let_ = Term::Let("a".to_string(), Box::new(int), Box::new(app));
        assert_eq!(let_.typeur(), Type::N)
    }

    #[test]
    fn type_fresh_var_test() {
        let x1 = Type::fresh_var();
        let x2 = Type::fresh_var();
        let x3 = Type::fresh_var();
        assert_ne!(x1, x2);
        assert_ne!(x1, x3);
        assert_ne!(x2, x3)
    }

    #[test]
    fn occur_check_test() {
        let x1 = Type::fresh_var();
        let x2 = Type::fresh_var();
        let a1 = Type::mk_arrow(x1, x2.clone());
        let a2 = Type::mk_arrow(x2.clone(), a1);
        let a3 = Type::mk_arrow(x2.clone(), x2);
        assert!(a2.occur_check(1));
        assert!(!a3.occur_check(1))
    }

    #[test]
    fn substitue_test() {
        let x1 = Type::fresh_var();
        let x2 = Type::fresh_var();
        let x3 = Type::fresh_var();
        let a1 = Type::mk_arrow(x1, x2.clone());
        let a2 = Type::mk_arrow(x2.clone(), a1);
        let sub = a2.substitue(1, x3.clone());
        let res = Type::mk_arrow(x2.clone(), Type::mk_arrow(x3.clone(), x2.clone()));
        let nosub = a2.substitue(42, x3);
        assert_eq!(sub, res);
        assert_eq!(a2, nosub)
    }

    fn test_sample(path: String) {
        println!("testing {} ...", &path);
        print!("Opening ... ");
        io::stdout().flush().unwrap();
        let prog_string = fs::read_to_string(path).unwrap();
        print!("OK\nParsing ... ");
        io::stdout().flush().unwrap();
        let prog: Term = *parser::TermParser::new().parse(&prog_string).unwrap();
        print!("OK\nTyping ... ");
        io::stdout().flush().unwrap();
        let type1 = prog.typeur();
        print!("OK\nReducing ... ");
        io::stdout().flush().unwrap();
        let reduced_prog = prog.evaluateur(200);
        print!("OK\nTyping reducded prog ... ");
        io::stdout().flush().unwrap();
        let type2 = reduced_prog.typeur();
        println!("OK");
        if type1 != type2 {
            println!(
                "Warning : prog type and reduced prog type are not the same ({}, {})",
                type1, type2
            )
        }
        println!()
    }

    #[test]
    fn samples_test() {
        let paths = fs::read_dir("./samples/").unwrap();
        for path in paths {
            test_sample(format!("{}", path.unwrap().path().display()));
        }
    }
}
