use crate::types::*;
use std::cell::RefCell;
use std::collections::HashMap;
use std::fmt::{Debug, Display, Formatter};

// TODO: remplacer panic par Result ?

/// Termes du langage
#[derive(Clone, PartialEq)]
pub enum Term {
    /// Variable (nom)
    Var(String),
    /// Application (fun, arg)
    App(Box<Term>, Box<Term>),
    /// Abstraction (var, corps)
    Abs(String, Box<Term>),
    /// Entier natif (val)
    Int(usize),
    /// Liste native (tête, queue)
    List(Box<Term>, Box<Term>),
    /// Liste vide
    EmptyList,
    /// Opérateur unaire (op, arg)
    Unop(UnOp, Box<Term>),
    /// Opérateur binaire (op, arg1, arg2)
    Binop(BinOp, Box<Term>, Box<Term>),
    /// If then else (cond, then, else)
    If(Box<Term>, Box<Term>, Box<Term>),
    /// If zero then else (cond, then, else)
    IfZer(Box<Term>, Box<Term>, Box<Term>),
    /// If empty then else (cond, then, else)
    IfEmp(Box<Term>, Box<Term>, Box<Term>),
    /// Recursive function (name, fun)
    Fix(String, Box<Term>),
    /// Affectation locale (nom, val, corps)
    Let(String, Box<Term>, Box<Term>),
    /// Let _ = ... in ... (val, corps)
    Let_(Box<Term>, Box<Term>),
    /// Booléens natifs (val)
    Bool(bool),
    /// Unit
    Unit,
    /// Région de la mémoire (adresse)
    Reg(usize),
}

// On implémente le trait Display pour Term pour pouvoir appeler println! aisément sur un terme
impl Display for Term {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Term::Var(s) => write!(f, "Var({})", s),
            Term::App(t1, t2) => write!(f, "App({}, {})", t1, t2),
            Term::Abs(s, t) => write!(f, "Abs({}, {})", s, t),
            Term::Int(n) => write!(f, "Int({})", n),
            Term::List(hd, tl) => write!(f, "List({}, {})", hd, tl),
            Term::EmptyList => write!(f, "[]"),
            Term::Unop(op, arg) => write!(f, "Unop:{}({})", op, arg),
            Term::Binop(op, arg1, arg2) => write!(f, "Binop:{}({}, {})", op, arg1, arg2),
            Term::If(c, t, e) => write!(f, "If({}, {}, {})", c, t, e),
            Term::IfZer(c, t, e) => write!(f, "IfZer({}, {}, {})", c, t, e),
            Term::IfEmp(c, t, e) => write!(f, "IfEmp({}, {}, {})", c, t, e),
            Term::Fix(n, fun) => write!(f, "Fix({}, {})", n, fun),
            Term::Let(s, v, b) => write!(f, "Let({}, {}, {})", s, v, b),
            Term::Let_(v, b) => write!(f, "Let(_, {}, {})", v, b),
            Term::Bool(b) => write!(f, "Bool({})", b),
            Term::Unit => write!(f, "()"),
            Term::Reg(a) => write!(f, "Reg({})", a),
        }
    }
}

// On implémente le trait Debug, nécessaire pour les méthodes assert! des tests, en s'appuyant sur Display
impl Debug for Term {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self)
    }
}

// Variable statique locale au thread
thread_local!(static VAR_COUNTER: RefCell<usize> = RefCell::new(0));
impl Term {
    // TODO: constructeurs pour alléger code
    // TODO: prettyprinter avec indentation
    /// Création d'une nouvelle variable unique
    pub fn fresh_var() -> Term {
        let num = VAR_COUNTER.with(|n| {
            *n.borrow_mut() += 1;
            *n.borrow()
        });
        let s = format!("x{}", num);
        Term::Var(s)
    }

    /// Alpha-convertit un terme en le transformant en un terme qui respecte une convention de Barendregt
    /// # Arguments
    /// * `mem` - Une mémoire maintenue pour gérer les références (association référence - terme)
    /// # Retour
    /// Un terme calqué sur celui de départ respectant une convention de Braendregt
    pub fn barendregt(&self, mem: &mut Mem) -> Term {
        match &self {
            Term::Var(_) | Term::Int(_) | Term::EmptyList | Term::Bool(_) | Term::Unit => {
                self.clone()
            }
            Term::App(t1, t2) => {
                let t3 = t1.barendregt(mem);
                let t4 = t2.barendregt(mem);
                Term::App(Box::new(t3), Box::new(t4))
            }
            Term::Abs(s1, bod) => {
                if let Term::Var(s2) = Term::fresh_var() {
                    let newbod = bod
                        .instantie(mem, s1, Term::Var(s2.clone()))
                        .barendregt(mem);
                    Term::Abs(s2, Box::new(newbod))
                } else {
                    unreachable!() // N'arrive jamais, fresh_var renvoie toujours un Var(s)
                }
            }
            Term::List(t1, t2) => {
                let t3 = t1.barendregt(mem);
                let t4 = t2.barendregt(mem);
                Term::List(Box::new(t3), Box::new(t4))
            }
            Term::Unop(op, arg) => {
                let arg1 = arg.barendregt(mem);
                Term::Unop(*op, Box::new(arg1))
            }
            Term::Binop(op, arg1, arg2) => {
                let t3 = arg1.barendregt(mem);
                let t4 = arg2.barendregt(mem);
                Term::Binop(*op, Box::new(t3), Box::new(t4))
            }
            Term::If(t1, t2, t3) => {
                let t4 = t1.barendregt(mem);
                let t5 = t2.barendregt(mem);
                let t6 = t3.barendregt(mem);
                Term::If(Box::new(t4), Box::new(t5), Box::new(t6))
            }
            Term::IfZer(t1, t2, t3) => {
                let t4 = t1.barendregt(mem);
                let t5 = t2.barendregt(mem);
                let t6 = t3.barendregt(mem);
                Term::IfZer(Box::new(t4), Box::new(t5), Box::new(t6))
            }
            Term::IfEmp(t1, t2, t3) => {
                let t4 = t1.barendregt(mem);
                let t5 = t2.barendregt(mem);
                let t6 = t3.barendregt(mem);
                Term::IfEmp(Box::new(t4), Box::new(t5), Box::new(t6))
            }
            Term::Fix(s1, bod) => {
                if let Term::Var(s2) = Term::fresh_var() {
                    let newbod = bod
                        .instantie(mem, s1, Term::Var(s2.clone()))
                        .barendregt(mem);
                    Term::Fix(s2, Box::new(newbod))
                } else {
                    unreachable!() // N'arrive jamais, fresh_var renvoie toujours un Var(s)
                }
            }
            Term::Let(s, t1, t2) => {
                let t3 = t1.barendregt(mem);
                let t4 = t2.barendregt(mem);
                Term::Let(s.clone(), Box::new(t3), Box::new(t4))
            }
            Term::Let_(t1, t2) => {
                let t3 = t1.barendregt(mem);
                let t4 = t2.barendregt(mem);
                Term::Let_(Box::new(t3), Box::new(t4))
            }
            Term::Reg(addr) => {
                let t = mem.get(addr).unwrap().to_owned();
                let newt = t.barendregt(mem);
                mem.assign(*addr, newt);
                self.clone()
            }
        }
    }

    /// Substitue une variable par un terme donné au sein d'un autre terme
    /// # Arguments
    /// * `mem` - Une mémoire maintenue pour gérer les références (association référence - terme)
    /// * `s` - L'identifiant associé à la variable à substituer
    /// * `a` - Le terme par lequel on substitue
    /// # Retour
    /// Un terme calqué sur celui de départ dans lequel la substitution a été effectuée
    pub fn instantie(&self, mem: &mut Mem, s: &str, a: Term) -> Term {
        match self {
            Term::Var(s1) => {
                if s == s1 {
                    a
                } else {
                    self.clone()
                }
            }
            Term::App(fun, arg) => {
                let newfun = fun.instantie(mem, s, a.clone());
                let newarg = arg.instantie(mem, s, a);
                Term::App(Box::new(newfun), Box::new(newarg))
            }
            Term::Abs(x, bod) => {
                let newbod = bod.instantie(mem, s, a);
                Term::Abs(x.to_string(), Box::new(newbod))
            }
            Term::Int(_) | Term::EmptyList | Term::Bool(_) | Term::Unit => self.clone(),
            Term::List(hd, tl) => {
                let newhd = hd.instantie(mem, s, a.clone());
                let newtl = tl.instantie(mem, s, a);
                Term::List(Box::new(newhd), Box::new(newtl))
            }
            Term::Unop(op, arg) => {
                let arg1 = arg.instantie(mem, s, a);
                Term::Unop(*op, Box::new(arg1))
            }
            Term::Binop(op, arg1, arg2) => {
                let t3 = arg1.instantie(mem, s, a.clone());
                let t4 = arg2.instantie(mem, s, a);
                Term::Binop(*op, Box::new(t3), Box::new(t4))
            }
            Term::If(c, t, e) => {
                let newc = c.instantie(mem, s, a.clone());
                let newt = t.instantie(mem, s, a.clone());
                let newe = e.instantie(mem, s, a);
                Term::If(Box::new(newc), Box::new(newt), Box::new(newe))
            }
            Term::IfZer(c, t, e) => {
                let newc = c.instantie(mem, s, a.clone());
                let newt = t.instantie(mem, s, a.clone());
                let newe = e.instantie(mem, s, a);
                Term::IfZer(Box::new(newc), Box::new(newt), Box::new(newe))
            }
            Term::IfEmp(c, t, e) => {
                let newc = c.instantie(mem, s, a.clone());
                let newt = t.instantie(mem, s, a.clone());
                let newe = e.instantie(mem, s, a);
                Term::IfZer(Box::new(newc), Box::new(newt), Box::new(newe))
            }
            Term::Fix(n, f) => Term::Fix(n.to_string(), Box::new(f.instantie(mem, s, a))),
            Term::Let(s1, val, bod) => {
                let newval = val.instantie(mem, s, a.clone());
                let newbod = bod.instantie(mem, s, a);
                Term::Let(s1.clone(), Box::new(newval), Box::new(newbod))
            }
            Term::Let_(val, bod) => {
                let newval = val.instantie(mem, s, a.clone());
                let newbod = bod.instantie(mem, s, a);
                Term::Let_(Box::new(newval), Box::new(newbod))
            }
            Term::Reg(addr) => {
                let t = mem.get(addr).unwrap().to_owned();
                let newt = t.instantie(mem, s, a);
                mem.assign(*addr, newt);
                self.clone()
            }
        }
    }

    /// Evalue un terme selon une stratégie left-to-right call-by-value en un nombre borné de tours
    /// # Arguments
    /// * `ctx` - Un contexte maintenu pour gérer les variables (association identifiant - terme)
    /// * `mem` - Une mémoire maintenue pour gérer les références (association référence - terme)
    /// * `cpt` - Etat courant du compteur de tours
    /// * `max` - Maximum du compteur de tours
    /// # Retour
    /// Le réduit d'un terme
    fn ltrcbv_etape(
        &self,
        ctx: &mut HashMap<&str, Term>,
        mem: &mut Mem,
        cpt: usize,
        max: usize,
    ) -> Term {
        // println!("({}) reducing {}", cpt, self);
        if cpt == max {
            self.clone()
        } else {
            let cpt = cpt + 1;
            match self {
                Term::App(fun, arg) => {
                    let newfun = fun.ltrcbv_etape(ctx, mem, cpt, max);
                    let newarg = arg.ltrcbv_etape(ctx, mem, cpt, max);
                    match newfun.clone() {
                        Term::Abs(x, bod) => bod
                            .instantie(mem, &x, newarg)
                            .ltrcbv_etape(ctx, mem, cpt, max),
                        Term::Fix(name, fun) => {
                            match *fun {
                                Term::Abs(x, bod) => {
                                    if let Term::Var(y) = Term::fresh_var() {
                                        let newfun2 = Term::Fix(
                                            name.clone(),
                                            Box::new(Term::Abs(
                                                y.clone(),
                                                Box::new(bod.instantie(mem, &x, Term::Var(y))),
                                            )),
                                        );
                                        let newbod = bod
                                            .instantie(mem, &name, newfun2)
                                            .instantie(mem, &x, newarg);
                                        newbod.ltrcbv_etape(ctx, mem, cpt, max)
                                    } else {
                                        unreachable!() // fresh_var() produit toujours un Var(y)
                                    }
                                }
                                _ => panic!("Can't apply {}, not a (recursive) function", newfun),
                            }
                        }
                        _ => panic!("Can't apply {}, not a function", newfun),
                    }
                }
                Term::Var(name) => match ctx.get(name.as_str()) {
                    None => panic!("Var {} not found in context", name),
                    Some(val) => val.clone(),
                },
                Term::Abs(_, _)
                | Term::Int(_)
                | Term::EmptyList
                | Term::Bool(_)
                | Term::Unit
                | Term::Fix(_, _) => self.clone(),
                Term::List(hd, tl) => {
                    let newhd = hd.ltrcbv_etape(ctx, mem, cpt, max);
                    let newtl = tl.ltrcbv_etape(ctx, mem, cpt, max);
                    Term::List(Box::new(newhd), Box::new(newtl))
                }
                Term::Unop(op, arg) => {
                    let newarg = arg.ltrcbv_etape(ctx, mem, cpt, max);
                    match format!("{}", op).as_str() {
                        "head" => match newarg {
                            Term::List(hd, _) => *hd,
                            Term::EmptyList => panic!("Can't give the head of an empty list"),
                            _ => panic!("Can't give head of {}, not a list", newarg),
                        },
                        "tail" => match newarg {
                            Term::List(_, tl) => *tl,
                            Term::EmptyList => panic!("Can't give the tail of an empty list"),
                            _ => panic!("Can't give tail of {}, not a list", newarg),
                        },
                        "not" => match newarg {
                            Term::Bool(b) => Term::Bool(!b),
                            _ => panic!("Can't negate {} (not boolean)", newarg),
                        },
                        "ref" => Term::Reg(mem.insert(newarg)),
                        "deref" => match newarg {
                            Term::Reg(addr) => mem.get(&addr).unwrap().clone(),
                            _ => panic!("Can't deref {}, not a ref", newarg),
                        },
                        _ => panic!("Unknown unary operator : {}", op),
                    }
                }
                Term::Binop(op, arg1, arg2) => {
                    let newarg1 = arg1.ltrcbv_etape(ctx, mem, cpt, max);
                    let newarg2 = arg2.ltrcbv_etape(ctx, mem, cpt, max);
                    match format!("{}", op).as_str() {
                        "add" => match (&newarg1, &newarg2) {
                            (Term::Int(n1), Term::Int(n2)) => Term::Int(n1 + n2),
                            (Term::Int(_), _) => panic!("Can't add, {} isn't an integer", newarg2),
                            (_, Term::Int(_)) => panic!("Can't add, {} isn't an integer", newarg1),
                            _ => panic!("Can't add, {} and {} aren't integers", newarg1, newarg2),
                        },
                        "sub" => match (&newarg1, &newarg2) {
                            (Term::Int(n1), Term::Int(n2)) => Term::Int(n1 - n2),
                            (Term::Int(_), _) => panic!("Can't sub, {} isn't an integer", newarg2),
                            (_, Term::Int(_)) => panic!("Can't sub, {} isn't an integer", newarg1),
                            _ => panic!("Can't sub, {} and {} aren't integers", newarg1, newarg2),
                        },
                        "cons" => {
                            // On ne vérifie pas que newarg2 soit bien une liste, la vérif sera faite au typage
                            Term::List(Box::new(newarg1), Box::new(newarg2))
                        }
                        "eq" => Term::Bool(newarg1 == newarg2),
                        "or" => match (&newarg1, &newarg2) {
                            (Term::Bool(b1), Term::Bool(b2)) => Term::Bool(*b1 || *b2),
                            (Term::Bool(_), _) => panic!("Can't or, {} isn't a boolean", newarg2),
                            (_, Term::Bool(_)) => panic!("Can't or, {} isn't a boolean", newarg1),
                            _ => panic!("Can't or, {} and {} aren't booleans", newarg1, newarg2),
                        },
                        "and" => match (&newarg1, &newarg2) {
                            (Term::Bool(b1), Term::Bool(b2)) => Term::Bool(*b1 && *b2),
                            (Term::Bool(_), _) => panic!("Can't and, {} isn't a boolean", newarg2),
                            (_, Term::Bool(_)) => panic!("Can't and, {} isn't a boolean", newarg1),
                            _ => panic!("Can't and, {} and {} aren't booleans", newarg1, newarg2),
                        },
                        "assign" => match &newarg1 {
                            Term::Reg(addr) => {
                                mem.assign(*addr, newarg2);
                                Term::Unit
                            }
                            _ => panic!("Can't assign, {} isn't a memory region", newarg1),
                        },
                        _ => {
                            panic!("Unknown binary operator : {}", op)
                        }
                    }
                }
                Term::If(c, t, e) => {
                    let newc = c.ltrcbv_etape(ctx, mem, cpt, max);
                    if newc == Term::Bool(true) {
                        t.ltrcbv_etape(ctx, mem, cpt, max)
                    } else {
                        e.ltrcbv_etape(ctx, mem, cpt, max)
                    }
                }
                Term::IfZer(c, t, e) => {
                    let newc = c.ltrcbv_etape(ctx, mem, cpt, max);
                    if newc == Term::Int(0) {
                        t.ltrcbv_etape(ctx, mem, cpt, max)
                    } else {
                        e.ltrcbv_etape(ctx, mem, cpt, max)
                    }
                }
                Term::IfEmp(c, t, e) => {
                    let newc = c.ltrcbv_etape(ctx, mem, cpt, max);
                    if newc == Term::EmptyList {
                        t.ltrcbv_etape(ctx, mem, cpt, max)
                    } else {
                        e.ltrcbv_etape(ctx, mem, cpt, max)
                    }
                }
                Term::Let(name, val, bod) => {
                    let newval = val.ltrcbv_etape(ctx, mem, cpt, max);
                    let mut newctx = ctx.clone();
                    newctx.insert(name, newval);
                    bod.ltrcbv_etape(&mut newctx, mem, cpt, max)
                }
                Term::Let_(val, bod) => {
                    val.ltrcbv_etape(ctx, mem, cpt, max);
                    bod.ltrcbv_etape(ctx, mem, cpt, max)
                }
                Term::Reg(addr) => {
                    let t = mem.get(addr).unwrap().to_owned();
                    t.ltrcbv_etape(ctx, mem, cpt, max)
                }
            }
        }
    }

    /// Evalue un terme selon une stratégie left-to-right call-by-value en un nombre borné de tours
    /// # Arguments
    /// * `max` - Nombre maximum de tours de l'évaluateur
    /// # Retour
    /// Le réduit du terme initial
    pub fn evaluateur(&self, max: usize) -> Term {
        self.ltrcbv_etape(&mut HashMap::new(), &mut Mem::new(), 0, max)
    }

    // TODO: Doc
    fn is_nonexpansive(&self) -> bool {
        match self {
            Term::Int(_) | Term::Bool(_) | Term::List(_, _) | Term::EmptyList => true,
            Term::Var(_) | Term::Reg(_) => true,
            Term::App(_, _) => false,
            Term::Abs(_, _) | Term::Fix(_, _) => true,
            Term::Unop(_, _) => false,
            Term::Binop(_, _, _) => false,
            Term::If(_, t, e) | Term::IfZer(_, t, e) | Term::IfEmp(_, t, e) => {
                t.is_nonexpansive() && e.is_nonexpansive()
            }
            Term::Let(_, val, bod) | Term::Let_(val, bod) => {
                val.is_nonexpansive() && bod.is_nonexpansive()
            }
            Term::Unit => true,
        }
    }

    /// Fonction de génération d'équations de types pour un terme
    /// # Arguments
    /// * `env` - Un environnement maintenu pour gérer les variables (association identifiant - type)
    /// * `mem` - Une mémoire maintenue pour gérer les références (association référence - terme)
    /// * `ty` - Le type cible courrant
    /// * `eq` - L'ensemble d'équations courrant
    /// # Retour
    /// Un vecteur contenant les équations (sous forme de couples `(a, b)` tels que `a = b` au sens d'équation de type) définissant le type du terme examiné
    fn gen_equas(
        &self,
        env: &HashMap<&str, Type>,
        mem: &mut Mem,
        ty: &Type,
        mut eq: Vec<Equation>,
    ) -> Vec<Equation> {
        // println!("Generating eqs for {}", self);
        // println!("env : {:?}\n", env);
        match self {
            Term::Var(s) => match env.get(s.as_str()) {
                None => {
                    panic!("Can't find a type for {}", self)
                }
                Some(t) => {
                    eq.push((ty.clone(), (*t).clone()));
                    eq
                }
            },
            Term::App(fun, arg) => {
                let ta = Type::fresh_var();
                let ta_t = Type::mk_arrow(ta.clone(), ty.clone());
                eq.push((Type::fresh_var(), ty.clone()));
                let tmp_eq = fun.gen_equas(env, mem, &ta_t, eq);
                arg.gen_equas(env, mem, &ta, tmp_eq)
            }
            Term::Abs(var, bod) => {
                let ta = Type::fresh_var();
                let tr = Type::fresh_var();
                eq.push((ty.clone(), Type::mk_arrow(ta.clone(), tr.clone())));
                let mut newenv = env.clone();
                newenv.insert(var, ta);
                bod.gen_equas(&newenv, mem, &tr, eq)
            }
            Term::Int(_) => {
                eq.push((ty.clone(), Type::N));
                eq
            }
            Term::List(hd, tl) => {
                let head_type = hd.typeur_aux(env, mem);
                eq.push((ty.clone(), Type::List(Box::new(head_type))));
                tl.gen_equas(env, mem, ty, eq)
            }
            Term::EmptyList => {
                eq.push((
                    ty.clone(),
                    Type::Forall(0, Box::new(Type::List(Box::new(Type::Var(0))))),
                ));
                eq
            }
            Term::Unop(op, arg) => {
                let op_type_pre = env
                    .get(&*format!("{}", op))
                    .expect("Unknown unary operator")
                    .clone();
                let op_type = match &op_type_pre {
                    Type::Arrow(_, _) => op_type_pre,
                    Type::Forall(x, inner) => inner.substitue(*x, Type::fresh_var()),
                    _ => panic!("Unrecognized unop type"),
                };
                if *op == UnOp::Ref {
                    mem.insert(*arg.clone());
                }
                if let Type::Arrow(arg_type, res_type) = op_type {
                    let mut neweq = eq.clone();
                    neweq.push((ty.clone(), *res_type));
                    arg.gen_equas(env, mem, &arg_type, neweq)
                } else {
                    unreachable!() // op_type est toujours de la forme (t_arg -> t_res)
                }
            }
            Term::Binop(op, arg1, arg2) => {
                let op_type_pre = env
                    .get(&*format!("{}", op))
                    .expect("Unknown binary operator")
                    .clone();
                let op_type = match &op_type_pre {
                    Type::Arrow(_, _) => op_type_pre,
                    Type::Forall(x, inner) => inner.substitue(*x, Type::fresh_var()),
                    _ => panic!("Unrecognized binop type"),
                };
                if let Type::Arrow(arg1_type, rtype) = op_type {
                    if let Type::Arrow(arg2_type, res_type) = *rtype {
                        let mut neweq = eq.clone();
                        neweq.push((ty.clone(), *res_type));
                        let tmp_eq = arg1.gen_equas(env, mem, &arg1_type, neweq);
                        arg2.gen_equas(env, mem, &arg2_type, tmp_eq)
                    } else {
                        unreachable!() // rtype est toujours de la forme (t_arg2 -> t_res)
                    }
                } else {
                    unreachable!() // op_type est toujours de la forme (t_arg1 -> (t_arg2 -> t_res))
                }
            }
            Term::If(c, t, e) => {
                let tmp_eq = t.gen_equas(env, mem, ty, eq);
                let tmp_eq2 = e.gen_equas(env, mem, ty, tmp_eq);
                c.gen_equas(env, mem, &Type::Bool, tmp_eq2)
            }
            Term::IfZer(c, t, e) => {
                let tmp_eq = t.gen_equas(env, mem, ty, eq);
                let tmp_eq2 = e.gen_equas(env, mem, ty, tmp_eq);
                c.gen_equas(env, mem, &Type::N, tmp_eq2)
            }
            Term::IfEmp(c, t, e) => {
                let tmp_eq = t.gen_equas(env, mem, ty, eq);
                let tmp_eq2 = e.gen_equas(env, mem, ty, tmp_eq);
                c.gen_equas(env, mem, &Type::Forall(0, Box::new(Type::Var(0))), tmp_eq2)
            }
            Term::Fix(name, fun) => {
                // On type fun avec un contexte dans lequel name a le type cible
                // Douteux ? On se donne comme hypothèse ce qu'on veut prouver
                let mut newenv = env.clone();
                newenv.insert(name, ty.clone());
                fun.gen_equas(&newenv, mem, ty, eq)
            }
            Term::Let(name, val, bod) => {
                let t = val.typeur_aux(env, mem);
                let mut newenv = env.clone();
                // println!("prev env = {:?}\n", env);
                newenv.insert(
                    name,
                    (if val.is_nonexpansive() {
                        t.generalise()
                    } else {
                        t
                    }), /*t.generalise()*/
                );
                // println!("new env = {:?}\n\n", newenv);
                bod.gen_equas(&newenv, mem, ty, eq)
            }
            Term::Let_(val, bod) => {
                let mut eq1 = val.gen_equas(env, mem, &Type::Unit, eq.clone());
                let mut eq2 = bod.gen_equas(env, mem, ty, eq);
                eq2.append(&mut eq1);
                eq2
            }
            Term::Bool(_) => {
                eq.push((ty.clone(), Type::Bool));
                eq
            }
            Term::Unit => {
                eq.push((ty.clone(), Type::Unit));
                eq
            }
            Term::Reg(addr) => {
                let t = match mem.get(addr) {
                    Some(t) => t.to_owned(),
                    None => panic!("Cannot find what {} refers to in memory", self),
                };
                let t_type = t.typeur_aux(env, mem);
                eq.push((ty.clone(), Type::Ref(Box::new(t_type))));
                eq
            }
        }
    }

    /// Génère le type d'un terme
    /// # Arguments
    /// * `env` - Un environnement maintenu pour gérer les variables (association identifiant - type)
    /// * `mem` - Une mémoire maintenue pour gérer les références (association référence - terme)
    /// # Retour
    /// Le type du terme donné
    fn typeur_aux(&self, env: &HashMap<&str, Type>, mem: &mut Mem) -> Type {
        let mut eqs = self.gen_equas(env, mem, &Type::fresh_var(), Vec::new());
        // println!("eqs : {:?}", eqs);
        // Après gen_equas, eqs contient au moins un élément
        let mut i = 0;
        let lim = 100;
        while eqs.len() > 1 && i < lim {
            eqs = unification_etape(eqs);
            i += 1;
        }
        if eqs.len() != 1 {
            panic!(
                "Unification takes too much time. {} equations after {} turns.",
                eqs.len(),
                lim
            )
        }
        // On pop la première (et dernière) équation
        let (mut left, mut right) = eqs.pop().unwrap();
        loop {
            match (&left, &right) {
                (Type::Arrow(_, _), Type::Arrow(_, _)) => {
                    println!("eqs1 = {:?}", eqs);
                    eqs.push((left.clone(), right.clone()));
                    println!("eqs2 = {:?}", eqs);
                    eqs = unification_etape(eqs);
                    println!("eqs3 = {:?}", eqs);
                    let (nl, nr) = eqs.pop().unwrap();
                    left = nl;
                    right = nr
                }
                (_, Type::N)
                | (_, Type::Bool)
                | (_, Type::Unit)
                | (_, Type::List(_))
                | (_, Type::Arrow(_, _))
                | (_, Type::Forall(_, _))
                | (_, Type::Ref(_)) => break,
                _ => {
                    eqs.push((left.clone(), right.clone()));
                    eqs = unification_etape(eqs);
                    let (nl, nr) = eqs.pop().unwrap();
                    left = nl;
                    right = nr
                }
            }
        }
        right
    }

    /// Génère le type d'un terme
    /// # Retour
    /// Le type du terme donné
    pub fn typeur(&self) -> Type {
        self.typeur_aux(&init_env(), &mut Mem::new())
    }
}

/// Représentation d'une mémoire
pub struct Mem {
    /// Buffer
    buf: HashMap<usize, Term>,
    /// Prochaine adresse vide
    next: usize,
}

impl Mem {
    /// Instanciation d'une mémoire vide
    pub(crate) fn new() -> Mem {
        Mem {
            buf: HashMap::new(),
            next: 0,
        }
    }

    /// Insertion d'un terme en mémoire
    /// # Arguments
    /// * `t` - Le terme à insérer
    /// # Retour
    /// L'adresse à laquelle le terme a été inséré
    fn insert(&mut self, t: Term) -> usize {
        self.buf.insert(self.next, t);
        let res = self.next;
        self.next += 1;
        res
    }

    /// Assignation d'un terme à une adresse mémoire
    /// # Arguments
    /// * `addr` - L'adresse à laquelle effectuer l'assignation
    /// * `t` - La valeur à assigner
    /// # Retour
    /// L'adresse à laquelle le terme a été assigné
    fn assign(&mut self, addr: usize, t: Term) -> usize {
        self.buf.insert(addr, t);
        addr
    }

    /// Récupération d'une valeur en mémoire
    /// # Arguments
    /// * `addr` - Adresse à laquelle se situe la valeur à récupérer
    /// # Retour
    /// `Some(val)` avec `val` la valeur recherchée, ou `None` si l'adresse n'est pas connue en mémoire
    fn get(&self, addr: &usize) -> Option<&Term> {
        self.buf.get(addr)
    }
}

/// Opérateurs unaires
#[derive(Eq, PartialEq, Copy, Clone)]
pub enum UnOp {
    /// Tête de liste
    Head,
    /// Queue de liste
    Tail,
    /// Négation
    Not,
    /// Création de référence
    Ref,
    /// Déréférencement
    Deref,
}

impl Display for UnOp {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            UnOp::Head => write!(f, "head"),
            UnOp::Tail => write!(f, "tail"),
            UnOp::Not => write!(f, "not"),
            UnOp::Ref => write!(f, "ref"),
            UnOp::Deref => write!(f, "deref"),
        }
    }
}

impl Debug for UnOp {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self)
    }
}

/// Opérateurs binaires
#[derive(Eq, PartialEq, Copy, Clone)]
pub enum BinOp {
    /// Addition
    Add,
    /// Soustraction
    Sub,
    /// Construction de liste
    Cons,
    /// Egalité
    Eq,
    /// Disjonction
    Or,
    /// Conjonction
    And,
    /// Assignation
    Assign,
}

impl Display for BinOp {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            BinOp::Add => write!(f, "add"),
            BinOp::Sub => write!(f, "sub"),
            BinOp::Cons => write!(f, "cons"),
            BinOp::Eq => write!(f, "eq"),
            BinOp::Or => write!(f, "or"),
            BinOp::And => write!(f, "and"),
            BinOp::Assign => write!(f, "assign"),
        }
    }
}

impl Debug for BinOp {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self)
    }
}

/// Génère l'environnement initial pour la génération d'équation, contenant les types des opérateurs primitifs
fn init_env<'a>() -> HashMap<&'a str, Type> {
    let mut env = HashMap::new();
    env.insert(
        "add",
        Type::mk_arrow(Type::N, Type::mk_arrow(Type::N, Type::N)),
    );
    env.insert(
        "sub",
        Type::mk_arrow(Type::N, Type::mk_arrow(Type::N, Type::N)),
    );
    env.insert("not", Type::mk_arrow(Type::Bool, Type::Bool));
    env.insert(
        "or",
        Type::mk_arrow(Type::Bool, Type::mk_arrow(Type::Bool, Type::Bool)),
    );
    env.insert(
        "and",
        Type::mk_arrow(Type::Bool, Type::mk_arrow(Type::Bool, Type::Bool)),
    );
    if let Type::Var(n) = Type::fresh_var() {
        env.insert(
            "cons",
            Type::Forall(
                n,
                Box::new(Type::mk_arrow(
                    Type::Var(n),
                    Type::mk_arrow(
                        Type::List(Box::new(Type::Var(n))),
                        Type::List(Box::new(Type::Var(n))),
                    ),
                )),
            ),
        );
        env.insert(
            "head",
            Type::Forall(
                n,
                Box::new(Type::mk_arrow(
                    Type::List(Box::new(Type::Var(n))),
                    Type::Var(n),
                )),
            ),
        );
        env.insert(
            "tail",
            Type::Forall(
                n,
                Box::new(Type::mk_arrow(
                    Type::List(Box::new(Type::Var(n))),
                    Type::List(Box::new(Type::Var(n))),
                )),
            ),
        );
        env.insert(
            "eq",
            Type::Forall(
                n,
                Box::new(Type::mk_arrow(
                    Type::Var(n),
                    Type::mk_arrow(Type::Var(n), Type::Bool),
                )),
            ),
        );
        env.insert(
            "ref",
            Type::Forall(
                n,
                Box::new(Type::mk_arrow(
                    Type::Var(n),
                    Type::Ref(Box::new(Type::Var(n))),
                )),
            ),
        );
        env.insert(
            "deref",
            Type::Forall(
                n,
                Box::new(Type::mk_arrow(
                    Type::Ref(Box::new(Type::Var(n))),
                    Type::Var(n),
                )),
            ),
        );
        env.insert(
            "assign",
            Type::Forall(
                n,
                Box::new(Type::mk_arrow(
                    Type::Ref(Box::new(Type::Var(n))),
                    Type::mk_arrow(Type::Var(n), Type::Unit),
                )),
            ),
        );
    } else {
        unreachable!() // N'arrive jamais, fresh_var renvoie toujours un Var(n)
    }
    env
}

/// Substitue une variable par un type donné au sein d'un ensemble d'équations de types
/// # Arguments
/// * `v` - Le nombre associé à la variable à substituer
/// * `ts` - Le type par lequel on substitue
/// * `eqs` - Les équations dans lesquelles on substitue
/// # Retour
/// Un vecteur d'équations dans lequel la substitution a été effectuée
fn substitue_partout(v: usize, ts: Type, eqs: &[Equation]) -> Vec<Equation> {
    let mut res = Vec::new();
    for (t1, t2) in eqs {
        let t3 = t1.substitue(v, ts.clone());
        let t4 = t2.substitue(v, ts.clone());
        res.push((t3, t4))
    }
    res
}

/// Effectue une étape d'unification.
/// Récupère (pop) la dernière équation `t1 = t2` du vecteur et applique la procédure suivante :
/// * Si t1 et t2 sont égaux (au sens sémantique), on se contente de renvoyer le reste du vecteur;
/// * Si t1 est une variable de type et n'apparaît pas dans t2, on renvoie le reste du vecteur dans lequel on a substitué t1 par t2;
/// * Inversement si t2 est une variable de type;
/// * Si les deux types sont des flèches (`ta -> tb` et `tc -> td`), on renvoie le reste du vecteur dans lequel on aura ajouté les équations `ta = tc` et `tb = td`
/// # Arguments
/// * `eqs` - Le vecteur d'équations de types considéré
/// # Retour
/// Un vecteur d'équations de types obtenu à partir de l'argument selon la procédure décrite
fn unification_etape(mut eqs: Vec<Equation>) -> Vec<Equation> {
    // On s'assure dans la foncion appelante que eqs soit non vide
    let (t1, t2) = eqs.pop().unwrap();

    // Si les deux types sont égaux, on se contente de pop
    if t1 != t2 {
        match (&t1, &t2) {
            (Type::Var(n), _) => {
                if t2.occur_check(*n) {
                    panic!("Invalid equation: {} contains {}", t2, t1)
                } else {
                    eqs = substitue_partout(*n, t2, &eqs)
                }
            }
            (_, Type::Var(n)) => {
                if t1.occur_check(*n) {
                    panic!("Invalid equation: {} contains {}", t1, t2)
                } else {
                    eqs = substitue_partout(*n, t1, &eqs)
                }
            }
            (Type::Arrow(t1, t2), Type::Arrow(t3, t4)) => {
                eqs.push((*t1.clone(), *t3.clone()));
                eqs.push((*t2.clone(), *t4.clone()))
            }
            (Type::Forall(n, t), _) => eqs.push((t.substitue(*n, Type::fresh_var()), t2)),
            (_, Type::Forall(n, t)) => eqs.push((t1, t.substitue(*n, Type::fresh_var()))),
            (Type::List(t1), Type::List(t2)) | (Type::Ref(t1), Type::Ref(t2)) => {
                eqs.push((*t1.clone(), *t2.clone()))
            }
            _ => panic!("Typing fail! Trying to match {} and {}", t1, t2),
        }
    }
    eqs
}
