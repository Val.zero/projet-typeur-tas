# Projet TAS - Typeur

Implémentation d'un typeur dans un langage au choix (ici Rust). Projet de l'UE Typage et Analyse Statique du Master 
d'informatique de Sorbonne Université (2021). J'ai implémenté toutes les fonctionnaltiés demandées jusqu'au polymorphisme
(voir le sujet pour plus de détails), ainsi que quelques extensions (booléens, parser).

## Utilisation

Pour recompiler le projet, utiliser `cargo build`; pour l'exécuter à partir des sources, `cargo run`; pour lancer les 
tests unitaires, `cargo test`. Pour toute autre utilisation, lancer l'exécutable `main`.

Afin de simplifier l'utilisation du programme, j'ai écrit un parser avec la bibliothèque [lalrpop](https://crates.io/crates/lalrpop).
Le typeur peut donc lire des programmes pré-écrits (voir la grammaire ci-dessous). Un certain nombre d'examples est fourni
dans le dossier `samples`. Pour lancer le typeur, on peut utiliser deux options : 
- `-f` en indiquant le chemin vers un fichier texte contenant le programme à typer
- `-s` en indiquant le numéro de l'exemple à lancer (dossier `samples`)

En l'absence d'option, le typeur s'exécute par défaut sur le premier exemple.

## Grammaire

Les programmes peuvent contenir les constructions suivantes : 
- **Entiers naturels** 
- **Booléens** : `true`, `false`
- **Identifiants** commençant par une lettre majuscule ou minuscule et pouvant contenir des chiffres
- **Listes** : liste vide `[]` ou non-vide `[<elem0>, ..., <elemN>]`
- **Lambda** : `(^<var>.(<body>))` avec `<var>` un identifiant de variable
- Opérateur de **point fixe** : `(rec^<fun>.(<lambda>))` avec `<fun>` un identifiant de fonction
- **Opérateurs unaires** (`<op> <arg>`): 
  - Tête/queue de liste : `head`/`tail`
  - Négation booléenne : `not`
- **Opérateurs binaires** (`<op> <arg1> <arg2>`):
  - Addition/soustraction d'entiers : `add`/`sub`
  - Constructeur de liste : `cons`
  - Test d'égalité : `eq`
  - Conjonction/disjonction booléenne : `and`/`or`
- Branchement **conditionnel** `if <cond> then <Texpr> else <Eexpr>` (où `<cond>` est réductible à un booléen, sachant que
les cas de tests d'égalité à 0 ou la liste vide sont traités à part afin de coller à l'énoncé du projet)
- **Affectation locale** : `let <var> = <val> in <body>`
- **Création d'une région mémoire** : `ref <expr>`
- **Déréférencement** d'une région mémoire : `!<var>`
- **Assignation** à une région mémoire : `assign <var> <expr>`